/* Create the PCStore database */
/* Script Data: January 26, 2017 */
/* Developped by Jun Yang */

/* Syntax: CREATE DATABASE Database_name */

create database pcstore_kd;

/* Switch tot he current database pcstore_kd */
use pcstore_kd;

/* Create the Defintion of the table customer */
create table Customers
(
    CustomerID varchar(5) not null, 
    CompanyName varchar(40) not null,
    ContactName varchar(30) null,
    ContackTitle varchar(30) null,
    Address varchar(60) null,
    City varchar(15) null,
    Region varchar(15) null,
    PostalCode varchar(10) null,
    Country varchar(15) null,
    Phone varchar(24) null,
    Fax varchar(24) null,
    constraint pk_Customers primary key (CustomerID asc)
);

/* Create the Defintion of the table orders */
drop table if exists Orders;
create table Orders 
(
	OrderID int(10) not null auto_increment,
    OrderDate datetime not null,
    CustomerID varchar(5) null,
    EmployeeID int(10) null,
    RequireDate datetime null,
    ShippedDate datetime null,
    ShipVia int(10) null,
    Freight decimal(15,4) null,
    ShipName varchar(40) null,
    ShipAddress varchar(60) null,
    ShipCity varchar(15) null,
    ShipRegion varchar(15) null,
    ShipPostalCode varchar(10) null,
    ShipCountry varchar(15) null,
    constraint pk_Orders primary key (OrderID asc) 
);

/* Create the Defintion of the table orderDetails */
/* if the Order Details tables exists, drop it and re-create it */
drop table if exists `Order Details`;
create table `Order Details` 
(
	OrderID int(10) not null,
    ProductID int(10) not null,
    UnitPrice decimal(10,4) null,
    Quantity smallint(5) null,
    Discount double(7,2) null,    
    constraint pk_OrderDetails primary key 
    (
		OrderID asc,
		ProductID asc
    ) 
);

/* Create the Defintion of the table Products */
drop table if exists `Products`;
create table Products 
(
	ProductID int not null auto_increment,
    ProductName varchar(40) not null,
    SupplierID int(10) null,
    CategoryID int(10) null,
    QuantityPerUnit varchar(20) null,
    UnitPrice decimal(10,4) null,
    UnitsInStock smallint(5) null,
    UnitsOnOder smallint(5) null,
    ReorderLevel smallint(5) null,
    Discontinued tinyint(1) null,    
    constraint pk_Products primary key (ProductID asc) 
);

/* Create the Defintion of the table Suppliers */
drop table if exists `Suppliers`;
create table Suppliers 
(
	SupplierID int not null auto_increment,
    CompanyName varchar(40) not null,
    ContactName varchar(30) null,
    ContactTitle varchar(30) null,
    Address varchar(60) null,
    City varchar(15) null,
    Region varchar(15) null,
    PostalCode varchar(10) null,
    Country varchar(15) null,
    Phone varchar(24) null,
    Fax varchar(24) null,
    HomePage  longtext null,    
    constraint pk_Suppliers primary key (SupplierID asc) 
);

/* Create the Defintion of the table Employees */
drop table if exists `Employees`;
create table Employees 
(
	EmployeeID int not null auto_increment,
    LastName varchar(20) not null,
    FirstName varchar(20) not null,
    Title varchar(30) null,
    TitleOfCourtesy varchar(10) null,
    BirthDate datetime null,
    HireDate datetime null,
    Address varchar(60) null,
    City varchar(15) null,
    Region varchar(15) null,
    PostalCode varchar(10) null,
    Country varchar(15) null,
    HomePhone varchar(24) null,
    Extension varchar(24) null,
    Photo longblob null,
    Notes longtext null,
    ReportsTo int null,   
    constraint pk_Employees primary key (EmployeeID asc) 
);

/* Create the Defintion of the table Shippers */
drop table if exists `Shippers`;
create table Shippers 
(
	ShipperID int not null auto_increment,
    CompanyName varchar(40) not null,
    Phone varchar(24) null,
    constraint pk_Shippers primary key (ShipperID asc) 
);

/* Create the Defintion of the table Categories */
drop table if exists `Categories`;
create table Categories 
(
	CategoryID int not null auto_increment,
    CategoryName varchar(10) not null,
    Description longtext not null,
    Picture longblob null,
    constraint pk_Categories primary key (CategoryID asc) 
);

/* create the foreign keys int table orders */
/* Syntax:
	[add] constraint fk_table1_table2 foreign key (column_name)references table_name (colunmn_name)
*/

/* 1) Betweenn Orders and Customers Tables */
alter table Orders
	add constraint fk_Orders_Customers foreign key (CustomerID) references Customers (CustomerID);

/* 2) Betweenn Orders and Shippers Tables */
alter table Orders
	add constraint fk_Orders_Shippers foreign key (ShipVia) references Shippers (ShipperID);

/* 3) Betweenn Orders and Employees Tables */
alter table Orders
	add constraint fk_Orders_Employees foreign key (EmployeeID) references Employees (EmployeeID);
    
select * from customers;
insert into customers(customerID,companyName) values ('00001', 'John Abbott');
delete from customers where customerID = '00001';