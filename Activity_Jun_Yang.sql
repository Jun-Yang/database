create table Customers 
(
	CustomerID int not null auto_increment primary key,
	LastName varchar(20) not null,
    FirstName varchar(20) not null,
    Address varchar(40) null,
    City varchar(15) null,
    State varchar(15) null,
    Zip varchar(10) null,
    Telephone varchar(24) null,
    Email varchar(40) null
);

create table Inventory 
(
	ItemID varchar(10) not null primary key,
	Description varchar(40) null,
    PricePerMonth decimal(7,2) null    
);

create table Rentals 
(
	RentalNum int not null auto_increment primary key,
	CustomerID int not null,
    DateRental date not null,
    DateReturn date null  
);

create table RentalLineItem 
(
	RentalID int not null,
	ItemID varchar(10) not null,
    Quantity int not null
    
);

alter table RentalLineItem
add constraint fk_RentalLineItem_RentalID foreign key (RentalID) references Rentals (RentalNum);

alter table RentalLineItem
add constraint fk_RentalLineItem_ItemID foreign key (ItemID) references Inventory (ItemID);

alter table RentalLineItem
add constraint pk_RentalLineItem_ItemID_RentalID primary key (RentalID,ItemID);

alter table Rentals
add constraint fk_Rentals_CustomerID foreign key (CustomerID) references Customers (CustomerID);

alter table Rentals
add constraint ck_Rentals_DateReturn check (DateReturn >= DateRental);

Drop table customer;
drop table inventory;
drop table rentals;
drop table rentallineitem;