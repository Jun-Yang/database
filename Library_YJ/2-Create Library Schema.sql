/* Create the Library schema
Script data: February 9, 2017
Developed by: Jun Yang
*/

-- Switch to the DVDRentals database
use Library
;
go

/* Syntax:
	Create schema schema_name authorization authorization_name
*/

create schema Person authorization dbo
;
go

create schema Item authorization dbo
;
go

create schema Loan authorization dbo
;
go

create schema Participant authorization dbo
;
go