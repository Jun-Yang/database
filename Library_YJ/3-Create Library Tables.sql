/* Create the Library tables
Script data: February 8, 2017
Developed by: Jun Yang
*/

-- Switch to the Library database
use Library
;
go

/* Syntax:
	create table table_name
	(
		column_name_1 data_type constraint,
		column_name_2 data_type constraint,
		...
		column_name_n data_type constraint,
	)
*/

/* Auto generated number:
	Access: AutoNumber
	MySql: auto_increment
	MySql Server: indentity(seed, increment)
	seed is the value that used for the very first row
	increment is the incremental value that is added to the identity of the previous row that was loaded
*/

/* check for existance of a specified object by verifying the table with an object ID
	if the table exists, it is deleted using the drop statement. Otherwise, it will be created 
*/
if OBJECT_ID('Participant.Members','U') is not null
	drop table Participant.Members
;
go

/*	Table No.1 - Participant.Members **/
create table Participant.Members
(
	MemberID int identity(1,1) not null,
	MemberFN varchar(20) not null,
	MemberMN varchar(20) null,
	MemberLN varchar(20) not null,
	Photo binary null,
	constraint pk_Members primary key clustered (MemberID asc)
)
;
go

/* return information about the Sales.Customers table */
execute sp_help 'Participant.Members'
;
go

/* Create the Lookup tables */

/**	Table No.2 - Person.Adults **/
create table Person.Adults
(
	MemberID int not null,
	Address varchar(40) not null,
	City varchar(20) not null,
	State char(2) not null,
	Zip char(5) not null,
	Phone varchar(24) not null,
	ExprDate date not null,	
	constraint pk_Adults primary key clustered (MemberID asc)
)
;
go

/**	Table No.3 - Person.Juveniles **/
create table Person.Juveniles
(
	MemberID int not null,
	JuvenFN varchar(20) not null,
	JuvenMN varchar(20) null,
	JuvenLN varchar(20) not null,	
	BirthDate date not null,	
	constraint pk_Juveniles primary key clustered (MemberID asc)
)
;
go

/**	Table No.4 - Item.Books **/
create table Item.Books
(
	ISBN varchar(15) not null,
	TitleID varchar(40) not null,
	PublisherID int not null,
	AuthorID int not null,
	DepID int not null,
	DateRsld date not null,
	constraint pk_Participants primary key clustered (ISBN asc)
)
;
go

/**	Table No.5 - Item.Departments **/
create table Item.Departments
(
	DepID smallint identity(1,1) not null,
	DepName varchar(20) not null,
	DepDescrip varchar(40) null,
	constraint pk_Departments primary key clustered (DepID asc)
)
;
go
/**	Table No.6 - Item.Titles **/
create table Item.Titles
(
	TitleID varchar(4) not null,
	Title varchar(40) not null,	
	Synopses text null,
	constraint pk_Titles primary key clustered (TitleID asc)
)
;
go

/**	Table No.7 - Participant.Authors **/
create table Participant.Authors
(
	AuthorID int identity(1,1) not null,
	AuthorFN varchar(20) not null,
	AuthorMN varchar(20) null,
	AuthorLN varchar(20) not null,
	Phone varchar(24) null,	
	constraint pk_Authors primary key clustered (AuthorID asc)
)
;
go

/**	Table No.8 - Participant.Publishers **/
create table Participant.Publishers
(
	PublisherID int identity(1,1) not null,
	PublisherName varchar(20) not null,
	Contact varchar(40) null,
	Title varchar(20) not null,
	Address varchar(40) not null,
	City varchar(20) not null,
	State char(2) not null,
	Zip char(5) not null,
	Phone varchar(24) null,	
	constraint pk_Publishers primary key clustered (PublisherID asc)
)
;
go

/**	Table No.9 - Loan.Transanctions **/
create table Loan.Transanctions
(
	TransID int identity(1,1) not null,
	BorrowID int not null,
	BookID smallint not null,
	DateOut datetime not null,
	DateDue datetime not null,
	DateIn datetime null,
	constraint pk_Transactions primary key clustered (TransID asc)	
)
;
go

/**	Table No.10 - Participant.Librians **/
create table Participant.Librians
(
	LibID smallint identity(1,1) not null,
	LibFN varchar(20) not null,
	LibMN varchar(20) null,	
	LibLN varchar(20) not null,
	Phone varchar(24) null,		
	constraint pk_Librians primary key clustered (LibID asc)
)
;
go

/*	Table No.11 - Loans.Borrows **/
create table Loan.Borrows
(
	BorrowID int identity(1,1) not null,
	MemID smallint not null,
	EmpID smallint not null,	
	constraint pk_Transactions primary key clustered (BorrowID asc)
)
;
go

/*	Table No.12 - Loans.Reservations **/
create table Loan.Reservations
(
	ReservID int identity(1,1) not null,
	MemID int not null,
	ISBN varchar(15) not null,
	LibID smallint not null,	
	constraint pk_Reservations primary key clustered (ReservID asc)
)
;
go

/*	Table No.13 - Loans.Availabilities **/
create table Loan.Availabilities
(
	ISBN varchar(15) not null,
	TransID int not null,
	ReservID int not null,	
	constraint pk_Reservations primary key clustered
	(
		ISBN, 
		TransID,
		ReservID
	)
)
;
go

/* display the user-defined and system table */
execute sp_tables
;
go