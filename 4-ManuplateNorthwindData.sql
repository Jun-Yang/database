/* Manuplate Northwind Database */
/* Script Data: January 26, 2017 */
/* Developped by Jun Yang */

/* Switch tot he current database Northwind2017 */
use Northwind2017;

/* display all mysql database int the server */
show databases;	

/* display the tables in Northwind2017 */
show tables;

/* retrive information about database objects*/

/* show create database <database_name> */
show create database Northwind2017;

/* show databases [like '<value>'] */
show databases like 'p%';

/* show column from <table_name> from <databse_name> [like '<value>'] */
show columns from Northwind2017.customers like 'c%';
show columns from customers from Northwind2017 like 'c%';

/* describe <table_name> [<column_name> | '<value>'] */
/* The same as last command */
describe suppliers;
describe orders;
show columns from orders;

/* show index from <table_name> [from <database_name>] */
show index from customers;

/* Retrieve the data from database */
/* 
<select_list> | <column_name> | <expression> [As <alias>]
from object_name
[where <search_condition>] 
*/

/* 1 understand where the data come from */
/* 2 select the object name */
/* 3 select the column name that will be returned in the result set  */
/* 4 add criterias one after another */
/* 5 group the data when used an aggregation function*/
/* 6 sort the data 

/* list countries where suppliers are located, sort the data */
select Country, CompanyName
from suppliers
order by Country asc,CompanyName asc;

/* return the name and location on suppliers from Germany */
select CompanyName, Address, City, Region
from suppliers
where Country='Germany';

select CompanyName, concat_ws(',', Address, City, Region) as Location
from suppliers
where Country in ('Germany','Sweden');

show columns from suppliers;
select * from suppliers;

/* return all information about the order details only for those orders placed after */

select  count(*)
from `order details`
where orderID > 11000;

select  *
from `order details`
where orderID > 11000;

/* find employees who were hired before */
select employeeID, concat(FirstName, ' ', LastName) as Name
from employees
where HireDate between '2005-4-1' and '2005-6-30';

select * from employees;

/* update data values */
set sql_safe_updates=0;
select * from orders;

Update Orders
set OrderDate = date_add(OrderDate, interval 9 year)
where OrderDate between '2006/1/1' and '2008/12/31';

Update Orders
set RequiredDate = date_add(RequiredDate, interval 9 year)	
where RequiredDate between '2006/1/1' and '2008/12/31';

Update Orders
set ShippedDate = date_add(ShippedDate, interval 9 year)	
where ShippedDate between '2006/1/1' and '2008/12/31';			

select * from employees;
UPDATE employees 
SET HireDate = date_sub(HireDate, INTERVAL 1 YEAR)
where employeeID = 9;    

/* suppose you dont remember a customer's company name that starts with 'the'  find all company names starting wiht 'the' */
select CompanyName as Company
from customers
where CompanyName like 'The%';

/* find suppliers int either UK or Paris */
select CompanyName as Company, Country,City
from suppliers
where Country='UK' or City='Paris';

SELECT CompanyName AS Company, Country, City
FROM suppliers
WHERE Fax is not null;

select * from suppliers;

/* check if order needed to be shipped today */
select OrderID,CustomerID,OrderDate
from orders
where RequiredDate=curdate();

/* find customers who are located in Seatle, kirkland or Portland*/
select CustomerID,CompanyName, city
from customers
where city in ('Seattle', 'Kirkland', 'Portland');

/* list all orders that have been shipped in the past 90 days */
select OrderID,CustomerID,OrderDate, ShippedDate
from orders 
where ShippedDate between date_sub(current_date(), INTERVAL 90 DAY) and current_date();

/* calculate field */
/* find out what all confection products (category id = 3) would cost if you raise the price by 25% */
select ProductID,ProductName,UnitPrice, UnitPrice * 1.25 as 'New Price'
from products
where CategoryID = 3; 

/* find out in what country all your customers located */
select distinct Country
from customers
order by country;

