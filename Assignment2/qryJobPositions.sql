/* Find how many types of jobs are offered at MonivOn? */
/* Script Data: January 31, 2017 */
/* Developped by Jun Yang */

select count(Distinct PositionID) as 'Number of Job'
from employees
;
