/* Find who is paid the lowest hourly rate? */
/* Script Data: January 31, 2017 */
/* Developped by Jun Yang */

select EmpID, concat(EmpFirst,' ',EmpLast) as 'Employee Name', min(HourlyRate) as 'Lowest Hourly Rate'
from employees
where HourlyRate !=0
;