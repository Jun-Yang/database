/* Find in what states or provinces do the employees reside? */
/* Script Data: January 31, 2017 */
/* Developped by Jun Yang */

select distinct State
from employees
order by State asc
;
