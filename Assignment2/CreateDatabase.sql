/* Create the MovinOn_YJ database */
/* Script Data: January 31, 2017 */
/* Developped by Jun Yang */

/* Syntax: CREATE DATABASE Database_name */
create database MovinOn_YJ;

/* Switch to the current database MovinOn_YJ */
use MovinOn_YJ;

/* Create the Defintion of the table customer */
drop table if exists Customers;
create table Customers
(
    CustID int not null auto_increment, 
    CompanyName varchar(40) not null,
    ContactFirst varchar(30) null,
    ContactLast varchar(30) null,
    Address varchar(60) null,
    City varchar(15) null,
    State varchar(2) null,
    Zip varchar(5) null,    
    Phone varchar(24) null,
    Balance int null,
    constraint pk_Customers primary key (CustID asc)
);

/* Create the Defintion of the table Employees */
drop table if exists Employees;
create table Employees 
(
	EmpID int not null auto_increment primary key,    
    EmpFirst varchar(20) not null,
    EmpLast varchar(20) not null,    
    Address varchar(60) null,
    City varchar(15) null,
    State varchar(2) null,
    Zip varchar(5) null,
    Phone varchar(24) null,
    Cell varchar(24) null,
    SSN varchar(15) null,    
    DOB date null,
    StartDate date null,
    EndDate date null,
    PositionID smallint null,
    Salary varchar(10) null,
    HourlyRate decimal(4,2) null,
    Review date null,    
    Memo longtext null,
    WarehouseID varchar(4) null          
);

/* Create the Defintion of the table Drivers */
drop table if exists Drivers;
create table Drivers 
(
	DriverID int not null auto_increment primary key,    
    DriverFirst varchar(20) not null,
    DriverLast varchar(20) not null,
    SSN varchar(15) null,    
    DOB date null,
    StartDate date null,
    EndDate date null,
    Address varchar(60) null,
    City varchar(15) null,
    State varchar(2) null,
    Zip varchar(5) null,
    Phone varchar(24) null,
    Cell varchar(24) null,
    MileageRate decimal(5,2) null,    
    Review longtext null,        
    DrivingRecord varchar(2) null      
);

/* Create the Defintion of the table orders */
drop table if exists Warehouses;
create table Warehouses 
(
	WarehouseID varchar(4) not null unique,    
    Address varchar(60) null,
    City varchar(15) null,
    State varchar(2) null,
    Zip varchar(5) null,
    Phone varchar(24) null,
    ClimateControl boolean null,
    SecurityGate boolean null,
    constraint pk_Warehouses primary key (WarehouseID asc) 
);

/* Create the Defintion of the table orders */
drop table if exists JobOrders;
create table JobOrders 
(
	JobID int(10) not null auto_increment primary key,    
    CustID int null,    
    MoveDate date null,
    FromAddress varchar(60) null,
    FromCity varchar(15) null,
    FromState varchar(2) null,
    ToAddress varchar(60) null,
    ToCity varchar(15) null,
    ToState varchar(2) null,    
    DistanceEst int(10) null,
    WeightEst int(10) null,    
    Packing boolean null,
    Heavy boolean null, 
    Storing boolean null        
);

/* Create the Defintion of the table Products */
drop table if exists Vehicles;
create table Vehicles 
(
	VehicleID varchar(7) not null unique primary key,
    LicensePlateNum varchar(10) not null,    
    Axle smallint null,    
    Color varchar(20) null        
);

/* Create the Defintion of the table Positions */
drop table if exists Positions;
create table Positions 
(
	PositionID smallint not null auto_increment primary key,
    Title varchar(20) null    
);

/* Create the Defintion of the table StorageUnits */
drop table if exists StorageUnits;
create table StorageUnits 
(
	WarehouseID varchar(4) not null,
    UnitID smallint not null,
    UnitSize varchar(10) not null,           
    Rent smallint null,
    primary key (WarehouseID, UnitID)    
);

/* Create the Defintion of the table JobDetails */
/* if the Job Details tables exists, drop it and re-create it */
drop table if exists JobDetails;
create table JobDetails 
(
	JobID int not null,
    VehicleID varchar(7) not null,
    DriverID int not null,
    MileageActual int(5) null,
    WeightActual int(5) null  
);

/* Create the Defintion of the table UnitRentals */
drop table if exists UnitRentals;
create table UnitRentals 
(
	CustID int not null, 
    WarehouseID varchar(4) not null,
    UnitID smallint not null,
    DateIn date not null,
    DateOut date null,
    constraint pk_UnitRentals primary key 
    (
		CustID, 
		WarehouseID,
		UnitID
    ) 
);