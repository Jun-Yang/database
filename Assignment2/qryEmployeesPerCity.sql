/* Find how many employees in each city? */
/* Script Data: January 31, 2017 */
/* Developped by Jun Yang */

select city, count(*) as 'Number of Employee'
from employees
group by city
;