/* Find who makes the highest salary? */
/* Script Data: January 31, 2017 */
/* Developped by Jun Yang */

select EmpID, concat(EmpFirst,' ',EmpLast) as 'Employee Name', max(Salary) as 'Highest Salary'
from employees
;