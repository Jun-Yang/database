/* Import data and insert data in MovinOn_YJ database */
/* Script Data: January 31, 2017 */
/* Developped by Jun Yang */

/* Switch to the current database MovinOn_YJ */
use MovinOn_YJ;

<<<<<<< HEAD:Database/Assignment2/AddData.sql
/* insert data into Positions table */
insert into positions (PositionID,Title)
values (1, 'Manager'),
       (2,'Clerk');

insert into positions (PositionID,Title)
values (3, 'Clerk1'),
       (4,'Clerk2'),
       (5, 'Clerk3'),       
       (6, 'Clerk4'),
       (7,'Clerk5');

/* insert data into Warehouses table */
insert into Warehouses (WarehouseID,Address,City,State,Zip,Phone,ClimateControl,SecurityGate)
values ('OR-1','4321 Sturgis','Jackson Hole','OR','83001','3075467135',1,0),
       ('WA-1','1901 Great Oaks Cove','Seattle','WA','98154','2063547987',1,1),
       ('WY-1','3987 NW 87th Street #8','Seattle','WY','98124','2063412024',0,1);
 
=======
>>>>>>> 4717532bdce23142db1edf883b267bb2fff59d64:Assignment2/AddData.sql
/* import date to database from Excel file */
/* import Employees table */
LOAD data local infile 'D:/MovinOn_Data/Employees.CSV'
INTO TABLE Employees
fields terminated by ','
enclosed by '"'
lines terminated by '\n'
ignore 1 rows;

/* import customers table */
LOAD data local infile 'D:/MovinOn_Data/Customers.CSV'
INTO TABLE customers
fields terminated by ','
enclosed by '"'
lines terminated by '\n'
ignore 1 rows;

/* import Drivers table */
LOAD data local infile 'D:/MovinOn_Data/Drivers.CSV'
INTO TABLE Drivers
fields terminated by ','
enclosed by '"'
lines terminated by '\n'
ignore 1 rows;

/* import vehicles table */
LOAD data local infile 'D:/MovinOn_Data/vehicles.CSV'
INTO TABLE vehicles
fields terminated by ','
enclosed by '"'
lines terminated by '\n'
ignore 1 rows;

/* import unitrentals table */
LOAD data local infile 'D:/MovinOn_Data/unitrentals.CSV'
INTO TABLE unitrentals
fields terminated by ','
enclosed by '"'
lines terminated by '\n'
ignore 1 rows;

/* import JobDetails table */
LOAD data local infile 'D:/MovinOn_Data/JobDetails.CSV'
INTO TABLE JobDetails
fields terminated by ','
enclosed by '"'
lines terminated by '\n'
ignore 1 rows;

/* import StorageUnits table */
LOAD data local infile 'D:/MovinOn_Data/StorageUnits.CSV'
INTO TABLE StorageUnits
fields terminated by ','
enclosed by '"'
lines terminated by '\n'
ignore 1 rows;

/* import JobOrders table */
LOAD data local infile 'D:/MovinOn_Data/JobOrders.CSV'
INTO TABLE JobOrders
fields terminated by ','
enclosed by '"'
lines terminated by '\n'
ignore 1 rows;

/* Query the each table */
select * from customers;
select * from drivers;
select * from vehicles;
select * from unitrentals;
select * from jobdetails;
select * from positions;
select * from storageunits;
select * from employees;
select * from joborders;