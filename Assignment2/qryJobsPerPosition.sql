/* Find how many people are employed in each type of job? */
/* Script Data: January 31, 2017 */
/* Developped by Jun Yang */

select PositionID, count(PositionID) as 'Number of Employee'
from employees
group by PositionID
;
