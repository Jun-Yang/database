/* Add Constraints in MovinOn_YJ database */
/* Script Data: January 31, 2017 */
/* Developped by Jun Yang */

/* Switch to the current database MovinOn_YJ */
use MovinOn_YJ;

<<<<<<< HEAD:Database/Assignment2/AddConstraints.sql
/* add constraints for table Drivers */
alter table Customers
alter Balance set default 0;

=======
>>>>>>> 4717532bdce23142db1edf883b267bb2fff59d64:Assignment2/AddConstraints.sql
/* add constraints for table Employees */
alter table Employees
add constraint fk_Positions_Employees foreign key (PositionID) references Positions(PositionID);

alter table Employees
add constraint fk_Warehouses_Employees foreign key (WarehouseID) references Warehouses(WarehouseID);

alter table Employees
add constraint ck_StartDate_Review check (Review >= StartDate);

alter table Employees
add constraint ck_StartDate_EndDate check (EndDate >= StartDate);

/* add constraints for table Drivers */
alter table Drivers
add constraint ck_StartDate_Review check (Review >= StartDate);

alter table Drivers
add constraint ck_StartDate_EndDate check (EndDate >= StartDate);

/* add constraints for table JobOrders */
alter table JobOrders
add constraint fk_Customers_JobOrders foreign key (CustID) references Customers(CustID); 

/* add constraints for table StorageUnits */
alter table StorageUnits
add constraint fk_Warehouses_StorageUnits foreign key (WarehouseID) references Warehouses(WarehouseID);

/* add constraints for table JobDetails */
alter table JobDetails
add constraint fk_Drivers_JobDetails foreign key (DriverID) references Drivers(DriverID);

alter table JobDetails
add constraint fk_Vehicles_JobDetails foreign key (VehicleID) references Vehicles(VehicleID);      