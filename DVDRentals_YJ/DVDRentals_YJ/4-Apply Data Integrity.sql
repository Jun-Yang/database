/* Apply Data Integrity to the DVD Rentals tables
Script data: February 8, 2017
Developed by: Jun Yang
*/

-- Switch to the DVDRentals database
use DVDRentals
;
go

/* Integrity Types:
	1. Domain (column)
	2. Entity (row)
	3. Referential (between 2 tables and columns)

	Constraint Types:
	1. Primary key (p)
	2. Unique (u_)
	3. Default (df_)
	4. Check (ck_)
	5. Foreign key (fk_)
*/

/* assuming we have a table named table1
	alter table table1
		add constraint constaint_lable constraint_type
*/

/* add the default value equals to 1 to the NumDisks column in the DVDs table */
alter table Production.DVDs
add constraint df_NumDisks_DVDs default (1) for NumDisks
;
go

/* add foreing key to DVDs table 
	1 between the DVDs and Formats tables 
*/

/* modify a column in a table */
alter table  Production.DVDs
alter column FormID char(2) not null
;
go

alter table Production.DVDs
add constraint fk_DVDs_Forms foreign key (FormID) references Production.Formats (FormID)
;
go

/* add foreing key to DVDs table 
	2 between the DVDs and Ratings tables 
*/

alter table Production.DVDs
add constraint fk_DVDs_Ratings foreign key (RatingID) references Production.Ratings (RatingID)
;
go
/* add foreing key to DVDs table 
	3 between the DVDs and MovieTypes tables 
*/

alter table Production.DVDs
add constraint fk_DVDs_MovieTypes foreign key (MTypeID) references Production.MovieTypes (MTypeID)
;
go
/* add foreing key to DVDs table 
	4 between the DVDs and Studios tables 
*/
alter table Production.DVDs
add constraint fk_DVDs_Studios foreign key (StudID) references Production.Studios (StudID)
;
go

/* add foreing key to DVDs table 
	5 between the DVDs and Status tables 
*/
alter table  Production.DVDs
alter column StatID char(3) not null
;
go

alter table Production.DVDs
add constraint fk_DVDs_Status foreign key (StatID) references Production.Status (StatID)
;
go

/* add foreing key to DVDParticipants table 
	1 between the DVDParticipants and DVDs tables 
*/
alter table Production.DVDParticipants
add constraint fk_DVDParticipants_DVDs foreign key (DVDID) references Production.DVDs (DVDID)
;
go

/* add foreing key to DVDParticipants table 
	2 between the DVDParticipants and Participants tables 
*/
alter table Production.DVDParticipants
add constraint fk_DVDParticipants_Participants foreign key (PartID) references Person.Participants (PartID)
;
go

/* add foreing key to DVDParticipants table 
	3 between the DVDParticipants and Participants tables 
*/
alter table Production.DVDParticipants
add constraint fk_DVDParticipants_Roles foreign key (RoleID) references Person.Roles (RoleID)
;
go

/* add foreing key to Sales.Orders table 
	1 between the Orders and Customers tables 
	2 between the Orders and Employees tables 
*/
alter table Sales.Orders
add constraint fk_Orders_Customers foreign key (CustID) references Sales.Customers (CustID),
    constraint fk_Orders_Employees foreign key (EmpID) references Person.Employees (EmpID)
;
go

/* add foreing key to Sales.Transactions table 
	1 between the Transactions and Orders tables 
	2 between the Transactions and DVDs tables 
*/
alter table Sales.Transactions
add constraint fk_Transactions_Orders foreign key (OrderID) references Sales.Orders (OrderID),
    constraint fk_Transactions_DVDs foreign key (DVDID) references Production.DVDs (DVDID)
;
go

/* set DVD Name constrainte */
alter table Production.DVDs
add constraint u_DVDName_DVDs unique (DVDName)    
;
go

/* set check constrainte on Due Date in the Sales.Transactions */
alter table Sales.Transactions
with nocheck
add constraint ck_DateDue_Transactions check (DateDue >= DateOut)    
;
go

/* set the pattern of a phone number (000) 000-0000  in the table Sales.Suppliers */
alter table dbo.table1
with check
add constraint ck_Phone_Suppliers check ([Phone Number] 
	like '([1-9][0-9][0-9]) [[0-9][0-9][0-9]-[0-9][0-9][0-9][0-9]')    
;
go

alter table dbo.table1
with check
add constraint ck_Phone_Suppliers check ([Phone Number] 
	like '(?:\(\d{3}\)|\d{3})[- ]?\d{3}[- ]?\d{4}')    
;
go

create table table1
([Phone Number] varchar(15) not null);


/* to transfer a table to a schema (change the schema of a table)
alter schema schema_name transfer schema.table_name


create table table1
(col1 int)
;
go

alter schema Person transfer dbo.table1
;
go

drop table Person.table1
;
go
*/