/* Create Indexes in the DVD Rentals tables
Script data: February 13, 2017
Developed by: Jun Yang
*/

-- Switch to the DVDRentals database
use DVDRentals
;
go

/* Syntax:
	1) create a nonclustered index on a table or view
		create nonclustered index index_name on table_name (column_name)
	2) create a clustered index on a table or view
		create clustered index index_name on table_name (column_name)	
*/

/* retrieve index information for the Sales.Customers */
exec sp_helpindex 'Sales.Customers'
;
go

/* index_id is unique only within the object. 

	object_id
*/ 
select name,			-- name of the index
	   index_id,		-- 0 = heap | 1 = clustered index | > 1 = nonclustered index
	   object_id,		-- 
	   type,			-- type of index
	   type_desc,		-- description of the index
	   is_unique,		-- 1 unique | 0 not unique
	   is_primary_key,  -- 1 index is part of the primary key
	   is_disabled      -- 1 index disabled | 0 is not disabled
from sys.indexes
where object_id = object_id('Sales.Customers')
;
go

select name 
from sys.indexes
;
go

/* modify the table Customers by adding the address columns */
alter table Sales.Customers
add 
	Address varchar(60) null,
	City nvarchar(15) null,
	Region nvarchar(15) null,
	PostCode nvarchar(10) null,
	Country nvarchar(20) null
;
go

select * from Sales.Customers;

/* add index to the city column in the Sales.Customers
*/

create index ix_Customers_City on Sales.Customers (City)
;
go

/* check the existance of indexes in the Person.Employees */
select name,			-- name of the index
	   index_id,		-- 0 = heap | 1 = clustered index | > 1 = nonclustered index
	   object_id,		-- 
	   type,			-- type of index
	   type_desc,		-- description of the index
	   is_unique,		-- 1 unique | 0 not unique
	   is_primary_key,  -- 1 index is part of the primary key
	   is_disabled      -- 1 index disabled | 0 is not disabled
from sys.indexes
where object_id = object_id('Person.Employees')
;
go

exec sp_helpindex 'Person.Employees'
;
go

/* Create a nonclustered index (ncl_LastName) on the employees last name column */
Create index ncl_LastName on Person.Employees (EmpLN)
;
go 

/* drop the nonclustered index ncl_LastName */
Drop index Person.Employees.ncl_LastName 
;
go

/* create a unique nonclustered index on Production.DVDs DVDName */
exec sp_helpindex 'Production.DVDs'
;
go

/* drop the u_ncl_DVDName index if exists and then re-create it */
if exists
(	select name 
	from sys.indexes 
	where name = 'u_ncl_DVDName'
)
drop index u_ncl_DVDName on Production.DVDs
;
go

create unique nonclustered index u_ncl_DVDName on Production.DVDs (DVDName)
;
go

/* create a nonclustered index on the Sales.Orders (CustID and EmpID) */
create unique nonclustered index u_ncl_CustID_EmpID on Sales.Orders (CustID, EmpID)
;
go

exec sp_helpindex 'Sales.Orders'
;
go