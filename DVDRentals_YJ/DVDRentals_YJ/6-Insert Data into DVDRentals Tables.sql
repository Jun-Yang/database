/* insert data into DVDRentals database */
use DVDRentals;
go

/* Inserts into the Roles table */
INSERT INTO Person.Roles (RoleID, RoleDescrip)
VALUES ('r101', 'Actor'),
('r102', 'Director'),
('r103', 'Producer'),
('r104', 'Executive Producer'),
('r105', 'Co-Producer'),
('r106', 'Assistant Producer'),
('r107', 'Screenwriter'),
('r108', 'Composer');
go

select * from Person.Roles;

exec sp_help 'Person.Roles'
;
go

/* Inserts into the MovieTypes table */
INSERT INTO Production.MovieTypes (MTypeID, MTypeDescrip)
VALUES ('mt10', 'Action'),
('mt11', 'Drama'),
('mt12', 'Comedy'),
('mt13', 'Romantic Comedy'),
('mt14', 'Science Fiction/Fantasy'),
('mt15', 'Documentary'),
('mt16', 'Musical');
go

select * from Production.MovieTypes
go

exec sp_help 'Production.MovieTypes'
go

/* Inserts into the Studios table */
INSERT INTO Production.Studios 
VALUES ('s101', 'Universal Studios'),
('s102', 'Warner Brothers'),
('s103', 'Time Warner'),
('s104', 'Columbia Pictures'),
('s105', 'Paramount Pictures'),
('s106', 'Twentieth Century Fox'),
('s107', 'Merchant Ivory Production');
go

select * from Production.Studios
go

/* Inserts into the Ratings table */
INSERT INTO Production.Ratings 
VALUES ('NR', 'Not rated'),
('G', 'General audiences'),
('PG', 'Parental guidance suggested'),
('PG13', 'Parents strongly cautioned'),
('R', 'Under 17 requires adult'),
('X', 'No one 17 and under');
go

select * from Production.Ratings
go

/* Inserts into the Formats table */
INSERT INTO Production.Formats 
VALUES ('f1', 'Widescreen'),
('f2', 'Fullscreen');
go

select * from Production.Formats
go

/* Inserts into the Status table */
INSERT INTO Production.Status 
VALUES ('s1', 'Checked out'),
('s2', 'Available'),
('s3', 'Damaged'),
('s4', 'Lost');
go

select * from Production.Status
go

exec sp_depends 'Person.Participants'

/* Inserts into the Particpants table */
INSERT INTO Person.Participants (PartFN, PartMN, PartLN)
VALUES ('Sydney', NULL, 'Pollack'),
('Robert', NULL, 'Redford'),
('Meryl', NULL, 'Streep'),
('John', NULL, 'Barry'),
('Henry', NULL, 'Buck'),
('Humphrey', NULL, 'Bogart'),
('Danny', NULL, 'Kaye'),
('Rosemary', NULL, 'Clooney'),
('Irving', NULL, 'Berlin'),
('Michael', NULL, 'Curtiz'),
('Bing', NULL, 'Crosby');
go

select * from Person.Participants
go

/* Inserts into the Employees table */
INSERT INTO Person.Employees (EmpFN, EmpMN, EmpLN)
VALUES ('John', 'P.', 'Smith'),
('Robert', NULL, 'Schroader'),
('Mary', 'Marie', 'Michaels'),
('John', NULL, 'Laguci'),
('Rita', 'C.', 'Carter'),
('George', NULL, 'Brooks');
go

select * from Person.Employees
go

/* Inserts into the Customers table */
INSERT INTO Sales.Customers (CustFN, CustMN, CustLN)
VALUES ('Ralph', 'Frederick', 'Johnson'),
('Hubert', 'T.', 'Weatherby'),
('Anne', NULL, 'Thomas'),
('Mona', 'J.', 'Cavenaugh'),
('Peter', NULL, 'Taylor'),
('Ginger', 'Meagan', 'Delaney');
go

select * from Sales.Customers
go

/* Inserts into the DVDs table */
INSERT INTO Production.DVDs (DVDName, NumDisks, YearRlsd, MTypeID, StudID, RatingID, FormID, StatID)
VALUES ('White Christmas', 1, '2000', 'mt16', 's105', 'NR', 'f1', 's1'),
('What''s Up, Doc?', 1, '2001', 'mt12', 's103', 'G', 'f1', 's2'),
('Out of Africa', 1, '2000', 'mt11', 's101', 'PG', 'f1', 's1'),
('The Maltese Falcon', 1, '2000', 'mt11', 's103', 'NR', 'f1', 's2'),
('Amadeus', 1, '1997', 'mt11', 's103', 'PG', 'f1', 's2'),
('The Rocky Horror Picture Show', 2, '2000', 'mt12', 's106', 'NR', 'f1', 's2'),
('A Room with a View', 1, '2000', 'mt11', 's107', 'NR', 'f1', 's1'),
('Mash', 2, '2001', 'mt12', 's106', 'R', 'f1', 's2');
go

select * from Production.DVDs
go

delete from Production.DVDs
dbcc checkident (DVDs, reseed, 0)

/* Inserts into the DVDParticipant table */
INSERT INTO Production.DVDParticipants
VALUES (3, 1, 'r102'),
(3, 4, 'r108'),
(3, 1, 'r103'),
(3, 2, 'r101'),
(3, 3, 'r101'),
(4, 6, 'r101'),
(1, 8, 'r101'),
(1, 9, 'r108'),
(1, 10, 'r102'),
(1, 11, 'r101'),
(1, 7, 'r101'),
(2, 5, 'r107');
go

select * from Production.DVDParticipants
go
 
exec sp_help 'Production.DVDParticipants'
go

/* Inserts into the Orders table */
INSERT INTO Sales.Orders (OrderID, CustID, EmpID)
VALUES (1, 1, 3),
(2, 1, 2),
(3, 2, 5),
(4, 3, 6),
(5, 4, 1),
(6, 3, 3),
(7, 5, 2),
(8, 6, 4),
(9, 4, 5),
(10, 6, 2),
(11, 3, 1),
(12, 1, 6),
(13, 5, 4);
go

select * from Sales.Orders
go

select * from Sales.Transactions
/* Inserts into the Transactions table */
Drop Table Sales.Transactions;
INSERT INTO Sales.Transactions (OrderID, DVDID, DateOut, DateDue)
VALUES (1, 1, GETDATE(), GETDATE()+3),
(1, 4, GETDATE(), GETDATE()+3),
(1, 8, GETDATE(), GETDATE()+3),
(2, 3, GETDATE(), GETDATE()+3),
(3, 4, GETDATE(), GETDATE()+3),
(3, 1, GETDATE(), GETDATE()+3),
(3, 7, GETDATE(), GETDATE()+3),
(4, 4, GETDATE(), GETDATE()+3),
(5, 3, GETDATE(), GETDATE()+3),
(6, 2, GETDATE(), GETDATE()+3),
(6, 1, GETDATE(), GETDATE()+3),
(7, 4, GETDATE(), GETDATE()+3),
(8, 2, GETDATE(), GETDATE()+3),
(8, 1, GETDATE(), GETDATE()+3),
(8, 3, GETDATE(), GETDATE()+3),
(9, 7, GETDATE(), GETDATE()+3),
(9, 1, GETDATE(), GETDATE()+3),
(10, 5, GETDATE(), GETDATE()+3),
(11, 6, GETDATE(), GETDATE()+3),
(11, 2, GETDATE(), GETDATE()+3),
(11, 8, GETDATE(), GETDATE()+3),
(12, 5, GETDATE(), GETDATE()+3),
(13, 7, GETDATE(), GETDATE()+3);
go

select * from Sales.Transactions
go


