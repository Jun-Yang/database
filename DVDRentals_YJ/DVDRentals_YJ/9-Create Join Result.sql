/* Query Multiple Tables in the DVD Rentals tables
Script data: February 14, 2017
Developed by: Jun Yang
*/

-- Switch to the DVDRentals database
use DVDRentals
;
go

/* return the DVD names, move types description, status description, studio, description
	and format description
*/
select D.DVDName as 'DVD Name', 
	   M.MTypeDescrip as 'Movie Type', 
       S.StatDescrip as 'Status', 
	   ST.StudDescrip as 'Studio', 
	   F.FormDescrip as 'Format'
from Production.DVDs as D
inner join Production.MovieTypes as M
on D.MTypeID = M.MTypeID
inner join Production.Status as S
on D.StatID = S.StatID 
inner join Production.Studios as ST
on D.StudID = ST.StudID 
inner join Production.Formats as F
on D.FormID = F.FormID
--where D.StatID IN ('S1', 'S3', 'S4')
;
go

/* return the Customer Full Name who place orders, Employee full name woh run the transanction, the order number, the transaction number, date out ,date due, date in and the DVD names rented
*/
select concat(C.CustFN, ' ', C.CustMN, ' ', C.CustLN) as 'Customer Full Name',
	   concat(E.EmpFN, ' ', E.EmpMN, ' ', E.EmpLN) as 'Employee Full Name',
	   O.OrderID, T.TransID, T.DateOut, T.DateDue, T.DateIn, D.DVDName as 'DVD Name'	   
from Sales.Customers as C
inner join Sales.Orders as O
on C.CustID = O.CustID
inner join Person.Employees as E 
on O.EmpID = E.EmpID
inner join Sales.Transactions as T
on O.OrderID = T.OrderID 
inner join Production.DVDs as D
on T.DVDID = D.DVDID 
;
go

/* return the full participant Name, role or roles he/she played in the movie
*/
select concat(P.PartFN, ' ', P.PartMN, ' ', P.PartLN) as 'Participant',
	   r.RoleDescrip as 'Role',
	   D.DVDName as 'DVD Name'	   
from Production.DVDParticipants as DP
inner join Person.Participants as P
on DP.PartID = P.PartID
inner join Person.Roles as R 
on DP.RoleID = R.RoleID
inner join Production.DVDs as D
on DP.DVDID = D.DVDID 
order by 'Participant'
;
go

use Northwind2014
;
go

/* return order ID and Customer ID for the most recent date 
	return a single value in the sub-select
*/
select OrderID, CustomerID, OrderDate 
from Sales.Orders
where OrderDate = (select max(OrderDate)
                   from Sales.Orders )
; 
go

/* return customer company name who place orders in the most recent date 
	return multiple values in the sub-select
*/
select C.CompanyName 
from Sales.Customers as C
where C.CustomerID in (select O.CustomerID
                   from Sales.Orders as O
				   where OrderDate between '1/1/2017' and '1/31/2017'
				   -- or where OrderDate between '2017-1-1' and '2017-1-31'
				   )
; 
go

/* find orderID CustomerID who placed more than 20 units of the product number 23 */
select OrderID, CustomerID  
from Sales.Orders as O
where 20 < (select Od.Quantity
            from Sales.[Order Details] as Od
			where O.OrderID = Od.OrderID and Od.ProductID = 23				   
			)
; 
go

/* Using exists and not exists 
	determing whether the data exists in a list of values
	return employee name who run orders after February 1, 2017
*/  
select concat(FirstName, ' ', LastName) as 'Employee Full Name'  
from HumanResources.Employees as E
where exists (select *
            from Sales.Orders as O
			where E.EmployeeID = O.EmployeeID 
				  and O.OrderDate >'2/1/2017'				   
			)
; 
go

select count(CustomerID) as 'Number of Customers' from Sales.Customers
;
go

select * from Sales.Customers
;
go

/* Built_In functions: 
	SUBSTING(EXPRESSION, START, LENGTH)
	LEFT(EXPRESSION,LENGTH)
	RIGHT(EXPRESSION,LENGTH)
*/

USE DVDRentals
;
GO

select DVDID,DVDName  
from Production.DVDs as D
order by DVDID 
;
go

/*return the string 'White', 'Christmas', and the string 'chris' 
  from the DVD name with DVD number equals 1
*/
select Left(DVDName,5) as 'Left String', 
	   Right(DVDName,9) as 'Right String', 
	   Substring(DVDName,7,5) as 'Substring' 
from Production.DVDs as D
where DVDID = 1
;
go

use Northwind2014
;
go 

/* return the customer company name, area code and phone number located in USA and Canada */
select CompanyName, Substring(Phone,2,3) as 'Area Code', Substring(Phone,7,8) as 'Phone'
from Sales.Customers
where [Country] in ('USA', 'Canada')

select CompanyName, Substring(Phone,2,2) as 'Area Code', Substring(Phone,6,8) as 'Phone'
from Sales.Customers 
where Country = 'Brazil'

/*
	Creater a temporary table
*/
select productname as 'Products',
	   unitprice as 'Price',
	   (unitprice * 1.1) as 'Tax'
into #pricetable
from Production.Products
;
go

select * from #pricetable
where Tax < 20
;
go 

/* create a temprary table named tempCustomers, assign the customer ID to be 3 characters from the first name and 2 characters from the last name, last name will be assign to company name, title to the Contact title address, city region and country to the respective column names
 */
if exists (#TempCustomers)
drop table #TempCustomers;
else
select upper(Substring([FirstName],1,3) + Lower(Substring([LastName],1,2))) as 'CustomerID',
       [LastName] as 'Company Name',
	   [Title] as 'ContactTitle',
	   [Address] as 'Address',
	   [City] as 'City',
	   [Region] as 'Region',
	   [Country] as 'Country'
into #TempCustomers
from [HumanResources].[Employees]
;
go

delete from #TempCustomers;
drop table #TempCustomers;
select * from #TempCustomers;
;
go
