/* Manipulate data in the DVD Rentals tables
Script data: February 14, 2017
Developed by: Jun Yang
*/

-- Switch to the DVDRentals database
use DVDRentals
;
go

/* To answer the question, follow these steps:
	1 Select object(s) needed to answer the questions (table, view or function)
	2 Select column(s) from the selected object(s)
	3 Run the script
	4 Define cirteria(s) and run them one after another
*/

/* list all DVD id, names and years release */
select DVDID, DVDName,YearRlsd
from Production.DVDS
;
go

/* list all DVD id, names and years release in 2001*/
select DVDID, DVDName,YearRlsd
from Production.DVDS
where YearRlsd = '2001'
;
go

/* list all DVD id, names and years release have more than one disk*/
select DVDID, DVDName, NumDisks, YearRlsd
from Production.DVDS
where NumDisks > 1
;
go

/* list all DVD id, names and years release have more than one disk with alias*/
select DVDID as 'ID', DVDName as 'Name', 
	   NumDisks as 'Number', YearRlsd as 'Year Released'
from Production.DVDS
where NumDisks > 1
;
go

/* list all employees with alias*/
select EmpID as 'Employee ID', EmpFN as 'First Name', 
	   EmpLN as 'Middle Name', EmpLN as 'Last Name' 
from Person.Employees as E
;
go

/* return the employees full name as a single string */
select EmpID as 'Employee ID', concat(EmpFN, ' ', EmpMN, ' ', EmpLN) as 'Full Name'
from Person.Employees as E
;
go

/* Upadat data:
	Syntax: update schema_name.table_name
			set column_name = expression	
*/
select * from [Sales].[Transactions];

/* change the DateIN to February 17 for the transaction ID number 1 */
update Sales.Transactions
	set DateIn = '2017/02/17'
    where TransID = 1
;
go

/* set the date in to date due + days for tans_id =2 */
update Sales.Transactions
	set DateIn = DateDue + 3
    where TransID = 2
;
go

/* return the loan time from each DVD in a number of day */
select TransID, DateOut, DateDue, DateIn,
      DateDiff(DAY, DateOut, DateIn) as 'Loan Time',
	  'Loan Hour Time ' as 'Empty',
	  DateDiff(HOUR, DateOut, DateIn) as 'Loan Hour Time'
from Sales.Transactions 
;
go

/* return the unique rating id from the table DVDs  */
select distinct RatingID     
from Production.DVDs
;
go

/* return the DVD names and movie types for the status id number S1, S3 and S4 */
select DVDName, MTypeID, StatID   
from Production.DVDs
where StatID IN ('S1', 'S3', 'S4')
;
go

/* return the DVD names and movie types for the status id number S1, S3 and S4 */
select DVDName, MTypeID, StatID   
from Production.DVDs
where StatID IN ('S1', 'S3', 'S4')
;
go

/* Join Syntax:
	select T1.F1, T1.F2, T2.F1, T2.F2
	from table_1 as T1 inner join table_2 as T2
	on T1.PK = T2.FK

	select T1.F1, T1.F2, T2.F1, T2.F2
	from table_1 as T1 left(right) outer join table_2 as T2
	on T1.PK = T2.FK

	select T1.F1, T1.F2, T2.F1, T2.F2
	from table_1 as T1 cross join table_2 as T2
	on T1.PK = T2.FK
*/

/* return the DVD names and movie types description and the status discription for the status number S1, S3 and S4 */
select D.DVDName, M.MTypeDescrip, S.StatDescrip   
from Production.DVDs as D
inner join Production.MovieTypes as M
on D.MTypeID = M.MTypeID
inner join Production.Status as S
on D.StatID = S.StatID 
where D.StatID IN ('S1', 'S3', 'S4')
;
go

/* set the date in to date due + days for tans_id =2 */
select * from Sales.Transactions	
;
go