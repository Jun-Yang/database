/* Create the DVD Rentals schema
Script data: February 8, 2017
Developed by: Jun Yang
*/

-- Switch to the DVDRentals database
use DVDRentals
;
go

/* Syntax:
	Create schema schema_name authorization authorization_name
*/

create schema Person authorization dbo
;
go

create schema Production authorization dbo
;
go

create schema Sales authorization dbo
;
go