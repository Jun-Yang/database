/* Create views in the DVD Rentals tables
Script data: February 16, 2017
Developed by: Jun Yang
*/

-- Switch to the DVDRentals database
use Northwind2014 
;
go

/* Create customer contact title view in the NorthWind Database */
if OBJECT_ID('Sales.CustomerContactTitleVeiw','V') is not null
	drop view Sales.CustomerContactTitleVeiw
;
go
 
create view Sales.CustomerContactTitleVeiw
as 
	select ContactTitle, ContactName
	from Sales.Customers	
;
go

/* Testing Sales.CustomerContactTitleVeiw */
select * from Sales.CustomerContactTitleVeiw
;
go

/*modify the veiw Sales.CustomerContactTitleVeiw to add company phone number */
alter view Sales.CustomerContactTitleVeiw
as 
select ContactTitle, ContactName, Phone as 'Company Phone Number'
	from Sales.Customers	
;
go

/*create a veiw HiredateView that provide name and hire date information 
for the employees of Northwind Traders */
if OBJECT_ID('HumanResources.HireDateView','V') is not null
	drop view HumanResources.HireDateView
;
go
 
create view HumanResources.HireDateView
as 
	select EmployeeID as 'Employee ID', 
	       (FirstName + ' ' + LastName) as 'Full Name', 
		   year(HireDate) as 'Hire Year'
	from HumanResources.Employees	
;
go

/* Testing HumanResources.HireDateView */
select * from HumanResources.HireDateView
;
go

/*create a veiw OrderLineView the order subtotal  */
if OBJECT_ID('Sales.OrderLineView','V') is not null
	drop view Sales.OrderLineView
;
go
 
create view Sales.OrderLineView
as 
	select  OD.OrderID, 	       
			convert(money,sum((OD.UnitPrice * OD.Quantity) * (1 - OD.Discount))) as 'SubTotal' 	       
	from Sales.[Order Details] as OD
	group by OD.OrderID
;
go

/* Testing HumanResources.HireDateView */
select * from Sales.OrderLineView
;
go

/* return the list of orders and their sum for a specific customer */
select  C.CompanyName, OLV.SubTotal 	       
from Sales.Customers as C
inner join Sales.Orders as O
on C.CustomerID = O.CustomerID
inner join Sales.OrderLineView as OLV
on O.OrderID = OLV.OrderID
Where O.OrderID between 10248 and 10250
order by OLV.SubTotal desc
;
go

/* Diaplay the definiton of the Sales.OrderLineView */
exec sp_helptext 'Sales.OrderLineView'
;
go

/* modify the Sales.OrderLineView */
alter view Sales.OrderLineView
with encryption
as 
	select  OD.OrderID, 	       
			convert(money,sum((OD.UnitPrice * OD.Quantity) * (1 - OD.Discount))) as 'SubTotal' 	       
	from Sales.[Order Details] as OD
	group by OD.OrderID
;
go

exec sp_helptext 'HumanResources.HireDateView'
;
go