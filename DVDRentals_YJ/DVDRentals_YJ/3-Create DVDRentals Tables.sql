/* Create the DVD Rentals tables
Script data: February 8, 2017
Developed by: Jun Yang
*/

-- Switch to the DVDRentals database
use DVDRentals
;
go

/* Syntax:
	create table table_name
	(
		column_name_1 data_type constraint,
		column_name_2 data_type constraint,
		...
		column_name_n data_type constraint,
	)
*/

/* Auto generated number:
	Access: AutoNumber
	MySql: auto_increment
	MySql Server: indentity(seed, increment)
	seed is the value that used for the very first row
	increment is the incremental value that is added to the identity of the previous row that was loaded
*/

/* check for existance of a specified object by verifying the table with an object ID
	if the table exists, it is deleted using the drop statement. Otherwise, it will be created 
*/
if OBJECT_ID('Sales.Customers','U') is not null
	drop table Sales.Customers
;
go

/*	Table No.1 - Sales.Customers **/
create table Sales.Customers
(
	CustID smallint identity(1,1) not null,
	CustFN varchar(20) not null,
	CustMN varchar(20) null,
	CustLN varchar(20) not null,
	constraint pk_Customers primary key clustered (CustID asc)
)
;
go

/* return information about the Sales.Customers table */
execute sp_help 'Sales.Customers'
;
go

/* Create the Lookup tables */

/**	Table No.2 - Person.Roles **/
create table Person.Roles
(
	RoleID varchar(4) not null,
	RoleDescrip varchar(30) not null,	
	constraint pk_Roles primary key clustered (RoleID asc)
)
;
go

/**	Table No.3 - Production.MovieTypes **/
create table Production.MovieTypes
(
	MTypeID varchar(4) not null,
	MTypeDescrip varchar(30) not null,	
	constraint pk_Participants primary key clustered (MTypeID asc)
)
;
go

/* modify the column MTypeDescrip to 40 character */
alter table Production.MovieTypes
alter column MTypeDescrip varchar(40) not null
;
go

/**	Table No.4 - Production.Studios **/
create table Production.Studios
(
	StudID varchar(4) not null,
	StudDescrip varchar(40) not null,	
	constraint pk_Studios primary key clustered (StudID asc)
)
;
go

/**	Table No.5 - Production.Ratings **/
create table Production.Ratings
(
	RatingID varchar(4) not null,
	RatingDescrip varchar(30) not null,	
	constraint pk_Ratings primary key clustered (RatingID asc)
)
;
go

/**	Table No.6 - Production.Formats **/
create table Production.Formats
(
	FormID varchar(3) not null,
	FormDescrip varchar(15) not null,	
	constraint pk_Formats primary key clustered (FormID asc)
)
;
go

/* modify the column FormID to 2 character */
/* step 1 drop the primary key */
alter table Production.Formats
drop pk_Formats
;
go

/* modify the column to char(2) */
alter table Production.Formats
alter column FormID char(2) not null
;
go

/* reset the primary key */
alter table Production.Formats
add constraint pk_Formats primary key (FormID)
;
go

/**	Table No.7 - Production.Status **/
create table Production.Status
(
	StatID char(3) not null,
	StatDescrip varchar(20) not null,	
	constraint pk_Status primary key clustered (StatID asc)
)
;
go

/* rename a database, table or column */
exec sp_renamedb 'old_name','new_name'
;
go

exec sp_rename 'schema_name.table_old_name','new_table_name'
;
go

exec sp_rename 'schema_name.table_old_column_name','new_column_name', 'COLUMN'
;
go

/*	Table No.8 - Sales.Transactions **/
create table Sales.Transactions
(
	TransID int identity(1,1) not null,
	OrderID int not null,
	DVDID smallint not null,
	DateOut datetime not null,
	DateDue datetime not null,
	DateIn datetime null,
	constraint pk_Transactions primary key clustered (TransID asc)
)
;
go

/**	Table No.9 - Person.Participants **/
create table Person.Participants
(
	PartID smallint identity(1,1) not null,
	PartFN varchar(20) not null,
	PartMN varchar(20) null,	
	PartLN varchar(20) not null,			
	constraint pk_Participants primary key clustered (PartID asc)
)
;
go

/* modify the PartMN to null if was to not null */
alter table Person.Participants
	alter column PartMN varchar(20) null
;
go

/**	Table No.10 - Person.Employees **/
create table Person.Employees
(
	EmpID smallint identity(1,1) not null,
	EmpFN varchar(20) not null,
	EmpMN varchar(20) null,	
	EmpLN varchar(20) not null,		
	constraint pk_Employees primary key clustered (EmpID asc)
)
;
go

/* Table No.11 Production.DVDs */
create table Production.DVDs
(
	DVDID smallint identity(1,1) not null,
	DVDName varchar(60) not null,
	NumDisks tinyint not null,
	YearRlsd Date not null,
	MTypeID varchar(4) not null,  -- Freign key
	StudID varchar(4) not null,   -- Freign key
	RatingID varchar(4) not null, -- Freign key
	FormID varchar(2) not null,   -- Freign key
	StatID varchar(3) not null,	  -- Freign key 
	constraint pk_DVDs primary key clustered (DVDID asc)
)
;
go

/* modify the data type of YearRlsd to char(4) */
alter table Production.DVDs
	alter column YearRlsd char(4) not null
;
go

/*	Table No.12 - Sales.Orders **/
create table Sales.Orders
(
	OrderID int not null,
	CustID smallint not null,
	EmpID smallint not null,	
	constraint pk_Transactions primary key clustered (OrderID asc)
)
;
go

/*	Table No.13 - Production.DVDParticipants **/
create table Production.DVDParticipants
(
	DVDID smallint not null,      -- Freign key
	PartID smallint not null,     -- Freign key
	RoleID varchar(4) not null,	  -- Freign key 
	constraint pk_DVDParticipants primary key clustered 
	(
		DVDID asc,
		PartID asc,
		RoleID asc
	)
)
;
go

/* display the user-defined and system table */
execute sp_tables
;
go