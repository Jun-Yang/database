/* Alter tables in the DVD Rentals tables
Script data: February 13, 2017
Developed by: Jun Yang
*/

-- Switch to the DVDRentals database
use DVDRentals
;
go

/* Syntax for modifying tables structure
	alter table schema_name.table_name 
	1) add a new column
	2) drop a column
	3) modify an existing column
	4) add constraints to a column
*/

/* adding a new column */
alter table schema_name.table_name
	add column_name data_type constraint(s)
;
go

/* drop a column */
alter table schema_name.table_name
	drop column column_name
;
go

/* add a check constraint to a existing table */
alter table schema_name.table_name
	with check with nocheck
	add constraint ck_constraint_name check (column1 > value)
;
go

/* add a default value to a existing column */
alter table schema_name.table_name	
	add constraint df_constraint_name default (default_value) for column_name
;
go

/* add a default value to a new column */
alter table schema_name.table_name
	add column_name data_type constraint(s)	
	df_constraint_name default (default_value)
;
go

/*Create a new table */
Create table Sales.MyTable
(
	column_a int,
	column_b varchar(20) null
)
;
go

/* return the definition of my */
execute sp_help 'Sales.MyTable'
;
go

/* remove column column_b */
alter table Sales.MyTable
	drop column column_b
;
go

/* add a new column column_c */
alter table Sales.MyTable
	add column_c int null
;
go

/* modify the data type of column_c to decimal */
alter table Sales.MyTable
	 alter column column_c decimal(5,2) null
;
go

/* add a check constraint to column_c, values > 2 */
alter table Sales.MyTable
	 add constraint ck_column_c_MyTable check (column_c > 2.00)
;
go

/* insert into schema_name.table_name(column1,column2,...)
	values( val1,val2, ...)
*/
insert into Sales.MyTable(column_a, column_c)
values(5,10)
;
go

 
select * from Sales.MyTable;

Bulk insert Sales.Mytable
From 'D:\data.csv'
with
(
	FirstRow = 2,
	FIELDTERMINATION = ','
);

Bulk insert Sales.Mytable
From 'D:\data.csv'
with(formatFile='c:\filename.csv')
(
	FirstRow = 2,
	FIELDTERMINATION = ','
);
