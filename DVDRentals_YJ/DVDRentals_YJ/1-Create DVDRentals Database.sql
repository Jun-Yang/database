/* Create the DVD Rentals dababase
Script data: February 8, 2017
Developed by: Jun Yang
*/
 
-- Switch to the master database
use master
;
go

-- Create DVD Rentals dababase
/* Syntax: create database database_name */

create database DVDRentals
on primary
(
	-- rows data name
	name = 'DVDRentals',
	-- rows path to data file
	filename = 'c:\database\Database_YJ\DVDRentals.mdf',
	-- rows data size
	size = 10MB,
	-- rows data autogrowth
	filegrowth = 1MB,
	-- rows maximum data size
	maxsize = 100MB
)
log on
(
	-- rows log file name
	name = 'DVDRentals_log',
	-- rows path to log file
	filename = 'c:\database\database_YJ\DVDRentals.ldf',
	-- rows log size
	size = 2MB,
	-- rows log autogrowth
	filegrowth = 10%,
	-- rows maximum log size
	maxsize = 25MB
)
;
go

-- return the information about the MyDB2 database
execute sp_helpDB DVDRentals
;
go

-- return the information about all the database
execute sp_helpDB
;
go