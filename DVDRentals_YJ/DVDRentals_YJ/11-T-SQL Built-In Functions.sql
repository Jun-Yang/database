/* Create Functions in the DVD Rentals tables
Script data: February 16, 2017
Developed by: Jun Yang
*/

-- Switch to the DVDRentals database
use Northwind2014 
;
go

/* Date and time Built-In functions */

/* return the year of the order */
select OrderID,
	   year(OrderDate) as 'Year', 
	   month(OrderDate) as 'Month',
	   day(OrderDate) as 'Day',
	   Datepart(weekday,OrderDate) as 'Weekday',
	   Datepart(dayofyear,OrderDate) as 'Day of year',
	   Datename(weekday,OrderDate) as 'Date name'
from Sales.Orders
--where year(OrderDate) between 2016 and 2017
where year(OrderDate) in(2016, 2017)
order by Datepart(year,OrderDate), Datepart(month,OrderDate),Datepart(day,OrderDate)
;
go

/* How long it took to ship an order */
select OrderID,OrderDate,RequiredDate,ShippedDate,
datediff(day,OrderDate,ShippedDate) as 'Days to ship'
-- datediff(hour,OrderDate,ShippedDate) as 'Hours to ship'
from Sales.Orders
;
go 

/* logical funcitons */

/* IsNumberic() return 1 if the expression convertable to any numeric type,
   otherwise it return 0 */

select EmployeeID,FirstName,LastName,PostalCode      
from HumanResources.Employees     
where ISNUMERIC(PostalCode) = 1
;
go


/* Immediate IIF(condition), true_value, falseValue */
/* Check if the product unit price is under 50, then display low price otherwise high price */
select ProductID,
       ProductName,
	   UnitPrice, 
       IIF(UnitPrice > 50,'High Price', 'Low Price') as 'Price Point'
from Production.Products 
order by UnitPrice
;
go

/* using the CASE clause to return a simple expression that determine the result */
select ProductID,
       ProductName,
	   UnitPrice, 
       'Price Range' = 
			case 
			when UnitPrice = 0 then 'Item not for resale'
			when UnitPrice < 50 then 'Unit price under $50'
			when UnitPrice between 50 and 250  then 'Unit price under $250'
			when UnitPrice between 250 and 1000  then 'Unit price under $1000'
			else 'Unit price over $1000'
	   end
from Production.Products 
order by UnitPrice
;
go

/* choose returns the item at the specified index from a list of values in SQL */
/* Syntax: choose(index, val1, val2, ...) */
select choose(2,'Manger','Director','Developer','Tester')
;
go

select CategoryID, CategoryName, choose(CategoryID, 'A','B','C','D','E','C','D','E') as 'Choose'
from Production.Categories 
;
go

/* isNull replaces null value with the specified replacement value */
/* Syntax: isNull(check_expression, replacement_value) */
/* return the customer address */
select CompanyName,Address,City,isNull(Region,'Unkown') as 'Region',PostalCode,Country
from Sales.Customers 
;
go

/* return the customer full address */
select CompanyName,
	   Address + ' ' + City + ' ' + Region + ' ' + PostalCode + ' ' + Country as 'Full Address'
from Sales.Customers 
;
go

/* return the customer full address using concat*/
select CompanyName,
	   concat(Address, ' ' , City, ' ', Region, ' ', PostalCode, ' ', Country) as 'Full Address'
from Sales.Customers 
;
go

/* return the customer full address using coalesce 
	the function evaluate the arguments in order and return the current value of the first expression that initially does not evaluate to null
*/
select CompanyName,
	   Address + ' ' + City + ' ' + coalesce(Region,' ') + ' ' + PostalCode + ' ' + Country 
	   as 'Full Address'
from Sales.Customers 
;
go

/* Aggregate functions: max | min | count | sum | avg | */
/* return the average unit price, minimum quality, maximu discount for order details */
select convert(money,avg(OD.UnitPrice)) as 'Average Price', 
	   min(OD.Quantity) as 'Minimum Quantity', 
	   max(OD.Discount) as 'Maximum Discount'
from [Sales].[Order Details] as OD
;
go

/* The sequence of sql statement execution 
	1 from
	2 where
	3 group by
	4 having
	5 select
	6 order by
*/

/* return only those customers that have placed more than 10 orders */
select C.CustomerID, C.CompanyName, count(O.OrderID)
from [Sales].[Orders] as O
inner join Sales.Customers as C
on O.CustomerID = C.CustomerID
group by C.CustomerID, C.CompanyName
having count(O.OrderID) > 10
;
go