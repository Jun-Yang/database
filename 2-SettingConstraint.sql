/* Set the constraint 
	primary key(pk) | foreign key(fk) | unique(u) | check(ck) | default(df)
*/

/* add a unique constraint to the CategoriesName in Categories table */
alter table Categories 
 add constraint u_CategoryName_Categories unique (CategoryName)
;

/* add a foreign key to the OrderDetails in OrderDetails table */
/* 1 Between the OrderDetails and Orders table*/
alter table `order details` 
 add constraint fk_OrderDetails_Orders foreign key (OrderID) references Orders (OrderID);
/* 2 Between the OrderDetails and Products table*/
alter table `order details`
 add constraint fk_OrderDetails_Products foreign key (ProductID) references Products (ProductID);
    
/* add foreign keys to the Orders table */
/* 1 Between the Orders and Customers table*/
alter table Orders 
 add constraint fk_Orders_Customers foreign key (CustomerID) references Customers (CustomerID);
/* 2 Between the Orders and shippers table*/
alter table Orders
 add constraint fk_Orders_Shippers foreign key (ShipVia) references Shippers (ShipperID);
/* 3 Between the Orders and Employees table*/
alter table Orders
 add constraint fk_Orders_Employees foreign key (EmployeeID) references Employees (EmployeeID);

/* add foreign keys to the Products table */
/* 1 Between the Products and Suppliers table*/
alter table Products 
 add constraint fk_Products_Suppliers foreign key (SupplierID) references Suppliers (SupplierID);
/* 2 Between the Products and Categories table*/
alter table Products
 add constraint fk_Products_Categories foreign key (CategoryID) references Categories (CategoryID);
 
/* Set the default value of country to 'USA' in Suppliers table*/
alter table Suppliers
 alter Country set default 'USA';
 
/* drop the default value of country to 'USA' in Suppliers table*/
alter table Suppliers
 alter Country drop default;
 
/* add a missing column in  Suppliers table*/
alter table Suppliers
 add Country varchar(40) null; 
 
/* add the check constraint to the order date in Orders table*/
alter table Orders
 add constraint ck_OrderDate_Orders check(OrderDate >= current_date()); 
 
alter table categories
	modify column CategoryName varchar(15);
    
/* to rename a column */
-- alter table table_name 
-- change old_name new_name data_type constraints    