/* create the 
employees 
table */

-- single line comment

Data Definition Language (DDL)
clause: CREATE | ALTER | DROP 
Syntax: create object_type object_name
	create table Employees

Modifying the strcuture of an object
Alter object_type object_name
	alter table Employees

DROP object_type object_name
	drop table employees



create table table_name
(
	column_name data_type constraint(s)
)

create table Employees
(
	EmployeeID int not null primary key,
	FirstName varchar(20) not null,
	MiddleName varchar(5) null,
	LastName  varchar(20) not null
)

create table OrderDetails 
(
	OrderID int not null,
	ProductID int not null,
	UnitPrice money not null,
	Quantity smallint not null,
	/* syntax constraint constraint_label constraint_type */
	constraint pk_OrderDetails primary key (
	OrderID asc,
	ProductID asc )
)










