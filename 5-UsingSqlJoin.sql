/* Using SQL join in Northwind Database */
/* Script Data: January 26, 2017 */
/* Developped by Jun Yang */

use northwind2017;

show tables;

/* return the customer company name and order placed by each of them */
select customers.CompanyName, orders.OrderID
from  customers
inner join orders
on customers.CustomerID = orders.CustomerID;

select C.CompanyName, O.OrderID
from  Customers as C
inner join Orders as O
on C.CustomerID = O.CustomerID;

/* list products supply by each supplier company name  return the ProductID, supplierID and supplier company name*/
select P.ProductID, P.ProductName, S.SupplierID, S.CompanyName
from  Products as P
inner join Suppliers as S
on P.SupplierID = S.SupplierID
order by P.ProductID;

select count(ProductID) from products;

/* check if any supplier did not supplied products */
select P.ProductID, P.ProductName, S.SupplierID, S.CompanyName
from  Products as P
right outer join Suppliers as S
on P.SupplierID = S.SupplierID
where P.ProductID is null;

/* check if any customer did not place order */
select C.CompanyName, O.OrderID
from  Customers as C
left outer join Orders as O
on C.CustomerID = O.CustomerID
where O.OrderID is null;

/* return total order placed by each customer */
select C.CustomerID, C.CompanyName as 'Customer', count(O.OrderID) as `Order Number`
from  Customers as C
join Orders as O
on C.CustomerID = O.CustomerID
group by C.CustomerID, C.CompanyName
order by `Order Number` desc;

/* the same function */
select C.CustomerID, C.CompanyName as 'Customer', count(O.OrderID) as `Order Number`
from  Customers as C
join Orders as O
on C.CustomerID = O.CustomerID
group by C.CustomerID, C.CompanyName
order by 3 desc;

/* find the highest unit price, lowest quantityperUnits and the average discount */
select max(UnitPrice) as 'Highest Unit Price', 
	   min(QuantityPerUnit) as 'Lowest quantity per Units', 
       avg(ReorderLevel) as 'Average Reorder Level'
from products;

/* find how many products in each category: return the category name and number of products */
select C.CategoryID, C.CategoryName as 'Category Name', 	   
       count(P.ProductID) as 'Product Number'
from products as P
join categories as C
on P.CategoryID = C.CategoryID
group by C.CategoryName;

/* find how many products in each category: return the category name and number of products for those categories that have more than 10*/
select C.CategoryID, C.CategoryName as 'Category Name', 	   
       count(P.ProductID) as `Product Number`
from products as P
join categories as C
on P.CategoryID = C.CategoryID
group by C.CategoryName
having `Product Number` > 10
;

/* some Sql funtions */
-- Greatest and least

select greatest(4,83,-1,0,15);
select least(4,83,-1,0,15);

-- Funciton coalesce return the first value in the list of argument that is not null
select coalesce(null,2,null,3);
select isnull(2 * null);

-- String comparison
select strcmp('big','big');

/* if() function compares 3 arguments
   if(exp1, exp2, exp3)
*/   
select ProductID,ProductName, if(UnitPrice < 10, 'Low Price', 'High Price') as `Price`
from products
order by `Price` desc;

/* Case statement */
select ProductName,UnitPrice,
	case 
		when UnitsInStock < 50 then 'Watch the stock'
        when UnitsInStock between 10 and 50 then 'Time to reorder'
    else 
		'Products is available'
    end as `Policy`    
from Products
order by ProductName	
;

/* convert value to a specified datatype: cast and convert 
	cast (expression as data_type
    convert(expression , data_type)
*/
select cast(20170121 as date);
select convert(20170121121212,datetime);

/* concatenation function
	concat(): return concatenated string
    concat_ws() - returns concatenated string with separator
*/

select concat('cat',' ','and',' ','dog') as 'concat',  
	   concat_ws(' ','cat','and','dog') as 'concat_ws';	
       
select concat(FirstName,' ', LastName) as 'Full name',
	   concat_ws(', ',Address,City,Region,PostalCode,Country) as 'Address'	
from employees
;

/* change word case functions:
	Lower(), LCase();
    Upper(), UCase();
*/
select CompanyName,lower(CompanyName),lcase(CompanyName), ucase(CompanyName),upper(CompanyName)
from Customers;

/* extract part of the value
	left(expression,length)
    right(expression,length)
    substing(string, start, length)
*/
select Productname,left(ProductName,4), substring(ProductName,1,4),substring(ProductName,11,8),right(ProductName,7) 
from products
where ProductID = 41
;

/* how many days did it take to ship an order */
select OrderID,OrderDate,RequiredDate,ShippedDate,
	daydiff(ShippedDate,OrderDate) as 'Number of Days'
from Orders
where Orderid > 10300
;