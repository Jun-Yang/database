/* Insert and Populate data in PCStore Database */
/* Script Data: January 31,  2017 */
-- Developped by Jun Yang

/* Switch to the specific database*/
use pcstore_kd;

-- Dumping data for table Shippers
insert into shippers (CompanyName, Phone)
values ('Speedy Express', '(503) 555-9831')
;

/* return the list of shippers */
select * from shippers;

/* return the list of categories */
select * from categories;

/* return the list of  suppliers*/
select * from suppliers;

/* return the list of products */
select * from products;

-- Dumping data for table Shippers
insert into shippers (CompanyName, Phone)
values
	('United Package', '(503) 555-3199'),
    ('Federal Shipping', '(503) 555-9988')
;

-- Insterting data into table Categories
insert into categories (CategoryName,Description,Picture)
values 
	('Beverages','Soft drinks, coffees, teas, beers, and ales','')    
;

insert into categories (CategoryName,Description,Picture)
values 
	('Condiments','Sweet and savory sauces, relishes, spreads, and seasonings',''),
    ('Confections','Desserts, candie and sweet breads','')
;

-- Insterting data into table suppliers
insert into suppliers (CompanyName,ContactName,ContactTitle,Address,City,Region,
						PostalCode,Country,Phone,Fax,HomePage)
values ('Exotic Liquids', 'Charlotte Cooper', 'Purchasing Manager','49 Gilbert St.',
        'London',null,'EC1 4SD', 'UK', '(171) 555-2222', null,null)	
;

insert into suppliers (CompanyName,ContactName,ContactTitle,Address,City,Region,
						PostalCode,Country,Phone,Fax,HomePage)
values ('New Orleans Cajun Delights', 'Shelley Burke', 'Order Adminisitrator','P.O.Box 78934',
        'New Orleans','LA','70117', 'USA', '(100) 555-7822', null,'www.cajun.com/index.html')	
;

/* Modify the data for SupplierID number */
update suppliers
set  CompanyName  = 'Tokyo Trader',
     ContactName  ='Yoshi Nagse',
     ContactTitle = 'Marketing Manager',
     Address = '9-B Sekimai',
     City = 'Tokyo',
     Region = null,
     PostalCode = null,
     Country = 'Japon',
     Phone = '(03) 35555-5011',
     HomePage = null
where SupplierID = 3;

-- Insterting data into table Categories
insert into products
values 
	(CategoryName)
;
