/* Create the constraints of tables in Library dababase
Script data: February 12, 2017
Developed by: Chen Chen, Wei Shao, Jun Yang
*/

use Library
;
go

/* Add foreign key constraint in table Person Adults */
ALTER TABLE Person.Adults 
ADD CONSTRAINT FK_Adults_Members FOREIGN KEY (MemberID)     
    REFERENCES Person.Members  (MemberID)         
;    
GO 

/* Add  foreign key constraint in table Person.Juveniles */
ALTER TABLE Person.Juveniles 
ADD CONSTRAINT FK_Juveniles_Members FOREIGN KEY (MemberID)     
    REFERENCES Person.Members  (MemberID)         
;    
GO

ALTER TABLE Person.Juveniles 
ADD CONSTRAINT FK_Juveniles_Adults FOREIGN KEY (AdultNo)     
    REFERENCES Person.Adults  (MemberID)         
;    
GO

/* Add  foreign key constraint in table Item.Items */
ALTER TABLE Item.Items     
ADD CONSTRAINT FK_Items_Titles FOREIGN KEY (TitleID)     
    REFERENCES Item.Titles (TitleID)     
;    
GO    

/* Add  foreign key constraint in table Item.Copies */
ALTER TABLE Item.Copies     
ADD CONSTRAINT FK_Copies_Titles FOREIGN KEY (TitleID)     
    REFERENCES Item.Titles (TitleID)     
;    
GO  

ALTER TABLE Item.Copies     
ADD CONSTRAINT FK_Copies_Items FOREIGN KEY (ISBN)     
    REFERENCES Item.Items (ISBN)     
;    
GO  

/* Add  foreign key constraint in table Loan.Reservations */
ALTER TABLE Loan.Reservations     
ADD CONSTRAINT FK_Reservations_Items FOREIGN KEY (ISBN)     
    REFERENCES Item.Items (ISBN)          
;    
GO  

ALTER TABLE Loan.Reservations     
ADD CONSTRAINT FK_Reservations_Members FOREIGN KEY (MemberID)     
    REFERENCES Person.Members  (MemberID)          
;    
GO  

/* Add  foreign key constraint in table Loan.Loans */
ALTER TABLE Loan.Loans     
ADD CONSTRAINT FK_Loans_Copies FOREIGN KEY (ISBN,CopyID)     
    REFERENCES Item.Copies (ISBN,CopyID)         
;    
GO  

ALTER TABLE Loan.Loans     
ADD CONSTRAINT FK_Loans_Members FOREIGN KEY (MemberID)     
    REFERENCES Person.Members  (MemberID)         
;    
GO  

ALTER TABLE Loan.Loans     
ADD CONSTRAINT FK_Loans_Titles FOREIGN KEY (TitleID)     
    REFERENCES Item.Titles (TitleID)        
;    
GO  

/* Add  foreign key constraint in table Loan.History */
ALTER TABLE Loan.History     
ADD CONSTRAINT FK_History_Copies FOREIGN KEY (ISBN,CopyID)     
    REFERENCES Item.Copies (ISBN,CopyID)         
;    
GO  

ALTER TABLE Loan.History     
ADD CONSTRAINT FK_History_Members FOREIGN KEY (MemberID)     
    REFERENCES Person.Members  (MemberID)         
;    
GO  

ALTER TABLE Loan.History     
ADD CONSTRAINT FK_History_Titles FOREIGN KEY (TitleID)     
    REFERENCES Item.Titles (TitleID)        
;    
GO  

exec sp_help 'item.copies';

