/* Create the tables in Library dababase
Script data: February 12, 2017
Developed by: Chen Chen, Wei Shao, Jun Yang
*/

use Library
;
go

if OBJECT_ID('Person.Members','U') is not null
	drop table Person.Members
;
go

/*	Table No.1 - Person.Members **/
create table Person.Members
(
	MemberID int identity(1,1) not null,
	LastName varchar(20) not null,
	FirstName varchar(20) null,
	MiddleInit char(1) not null,	
	Photo binary null,
	constraint pk_Members primary key clustered (MemberID asc)
)
;
go

/* return information about the Sales.Customers table */
execute sp_help 'Person.Members'
;
go

/* Create the Lookup tables */

/**	Table No.2 - Person.Adults **/
create table Person.Adults
(
	MemberID int not null,
	Street varchar(40) not null,
	City varchar(20) not null,
	State char(2) not null,
	Zip char(5) not null,
	Phone varchar(24) not null,	
	ExpireDate date not null,			
	constraint pk_Adults primary key clustered (MemberID asc)
)
;
go

/**	Table No.3 - Person.Juveniles **/
create table Person.Juveniles
(
	MemberID int not null,
	AdultNo int not null,
	BirthDate date not null,	
	constraint pk_Juveniles primary key clustered (MemberID asc)
)
;
go

/**	Table No.4 - Item.Titles **/
create table Item.Titles
(
	TitleID int identity(1,1) not null,
	Title varchar(40) not null,	
	Author varchar(20) not null,
	Synopsis text null,
	constraint pk_Titles primary key clustered (TitleID asc)
)
;
go

/**	Table No.5 - Item.Books **/
create table Item.Items
(
	ISBN int identity(1,1) not null,
	TitleID int not null,
	Language varchar(20) not null,	
	Cover varchar(20) not null,	
	Loanable  char(1) not null,
	constraint pk_Participants primary key clustered (ISBN asc)
)
;
go

/**	Table No.5 - Item.Copies **/
create table Item.Copies
(
	ISBN int not null,
	CopyID int  not null,	
	TitleID int not null,
	OnLoan char(1) not null,
	constraint pk_Copies primary key clustered (ISBN,CopyID)
)
;
go

/**	Table No.7 - Loan.Transanctions **/
create table Loan.Loans
(
	ISBN int not null,
	CopyID int not null,		
	TitleID int not null,
	MemberID int identity(1,1) not null,
	DateOut date not null,
	DateDue date not null,	
	constraint pk_Transactions primary key clustered (ISBN,CopyID)	
)
;
go

/*	Table No.8 - Loans.Histories **/
create table Loan.History
(
	ISBN int not null,
	CopyID int not null,	
	DateOut date not null,	
	TitleID int not null,
	MemberID int identity(1,1) not null,	
	DateDue date not null,
	DateIn date not null,	
	FineAssessed money null,
	FinePaid money null,
	FineWaived money null,
	Remarks text null
	constraint pk_History primary key clustered (ISBN,CopyID)	
)
;
go

/*	Table No.9 - Loans.Reservations **/
create table Loan.Reservations
(
	ISBN int not null,
	MemberID int not null,	
	LogDate date not null,
	State char(2) null,
	Remarkds text null	
	constraint pk_Reservations primary key clustered (ISBN, MemberID)
)
;
go

/**	Table No.10 - Participant.Authors **/
create table Participant.Authors
(
	AuthorID int identity(1,1) not null,
	AuthorFN varchar(20) not null,
	AuthorMN varchar(20) null,
	AuthorLN varchar(20) not null,
	Phone varchar(24) null,	
	constraint pk_Authors primary key clustered (AuthorID asc)
)
;
go

/**	Table No.11 - Participant.Publishers **/
create table Participant.Publishers
(
	PublisherID int identity(1,1) not null,
	PublisherName varchar(20) not null,
	Contact varchar(40) null,
	Title varchar(20) not null,
	Address varchar(40) not null,
	City varchar(20) not null,
	State char(2) not null,
	Zip char(5) not null,
	Phone varchar(24) null,	
	constraint pk_Publishers primary key clustered (PublisherID asc)
)
;
go

/**	Table No.12 - Participant.Librians **/
create table Participant.Librians
(
	LibID smallint identity(1,1) not null,
	LibFN varchar(20) not null,
	LibMN varchar(20) null,	
	LibLN varchar(20) not null,
	Phone varchar(24) null,		
	constraint pk_Librians primary key clustered (LibID asc)
)
;
go

/*	Table No.13 - Loans.Availabilities **/
create table Loan.Availabilities
(
	ISBN varchar(15) not null,
	TransID int not null,
	ReservID int not null,	
	CopyID smallint not null,
	constraint pk_Availabilities primary key clustered
	(
		ISBN, 
		TransID,
		ReservID,
		CopyID
	)
)
;
go

