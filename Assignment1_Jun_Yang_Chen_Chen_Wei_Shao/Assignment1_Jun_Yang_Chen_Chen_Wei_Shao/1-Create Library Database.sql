/* Create the Library dababase
Script data: February 12, 2017
Developed by: Chen Chen, Wei Shao, Jun Yang
*/

-- Switch to the master database
use master
;
go

-- Create Library dababase
/* Syntax: create database database_name */

create database Library
on primary
(
	-- rows data name
	name = 'Library',
	-- rows path to data file
	filename = 'C:\Library.mdf',
	-- rows data size
	size = 10MB,
	-- rows data autogrowth
	filegrowth = 1MB,
	-- rows maximum data size
	maxsize = 100MB
)
log on
(
	-- rows log file name
	name = 'Library_log',
	-- rows path to log file
	filename = 'C:\Library_log.ldf',
	-- rows log size
	size = 2MB,
	-- rows log autogrowth
	filegrowth = 10%,
	-- rows maximum log size
	maxsize = 25MB
)
;
go

-- return the information about the MyDB2 database
execute sp_helpDB Library
;
go

-- return the information about all the database
execute sp_helpDB
;
go