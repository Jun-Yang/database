/* Create the Schema in Library dababase
Script data: February 12, 2017
Developed by: Chen Chen, Wei Shao, Jun Yang
*/

use Library
;
go

/* Syntax:
	Create schema schema_name authorization authorization_name
*/

create schema Person authorization dbo
;
go

create schema Item authorization dbo
;
go

create schema Loan authorization dbo
;
go

create schema Participant authorization dbo
;
go

